using UnityEngine;
//using System.Collections;
using ExitGames.Client.Photon;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    //singleton dec
    #region
    private static GameManager instance = null;

    public static GameManager Instance
    {
        get { return instance; }
    }

    private void Awake ()
    {
        if ( instance )
        {
            DestroyImmediate( gameObject );
            return;
        }
        instance = this;

        DontDestroyOnLoad( gameObject );
    }

    #endregion

    [Header( "Game Version Number" )]
    [SerializeField]
    public string Version;

    [Header( "Other Variables" )]
    public double timeLeft = 30f;
    public double timestore;
    public bool loaded = false;
    public double remainingTime;

    string redTeamKey = "redTeamKey";
    string greenTeamKey = "greenTeamKey";

    

    

    // Variables
    #region
    public GameObject redCube;
    public GameObject greenCube;

    public bool justLoaded = true;

    //public int SecondsPerTurn = 5;                  // time per round/turn
    public double StartTime;                        // this should could also be a private. i just like to see this in inspector
    public Rect TextPos = new Rect( 0, 80, 150, 300 );   // default gui position. inspector overrides this!

    public bool startRoundWhenTimeIsSynced;        // used in an edge-case when we wanted to set a start time but don't know it yet.
    private const string StartTimeKey = "st";       // the name of our "start time" custom property.
    ExitGames.Client.Photon.Hashtable startTimeProp = new Hashtable();
    public string playerName;

    public AudioClip pickup;

    public int _redTeamScore = 0;
    public int _greenTeamScore = 0;

    #endregion

    void Start ()
    {
        timestore = timeLeft;
    }

    // Update is called once per frame
    void Update ()
    {

        //if we're in main scene
        if ( SceneManager.GetActiveScene().name == "Lobby" )
        {
            if(Cursor.visible == false)
                Cursor.visible = true;
            if(Cursor.lockState == CursorLockMode.Locked)
                Cursor.lockState = CursorLockMode.None;
        }
        if ( SceneManager.GetActiveScene().name == "scene1" )
        {

            if ( Cursor.visible == true )
            {
                //if cursors visiible hide it and lock it to screen
                Cursor.visible = false;
                Cursor.lockState = CursorLockMode.Locked;
            }
            if ( loaded )
            {
                double elapsedTime = (PhotonNetwork.time - StartTime);
                remainingTime = timeLeft - (elapsedTime % timeLeft);
                if ( PhotonNetwork.isMasterClient )
                {
                    SpawnCube();
                }
                if ( remainingTime <= 1 )
                {
                    //time has elapsed load Lobby again
                    SceneManager.LoadScene( "Lobby" );
                    PhotonNetwork.Disconnect();
                    timeLeft = timestore;
                    remainingTime = 0f;
                    loaded = false;
                    startTimeProp.Clear();
                    justLoaded = true;
                    _greenTeamScore = 0;
                    _redTeamScore = 0;
                }
            }
        }

        if ( startRoundWhenTimeIsSynced )
        {
            this.StartRoundNow();   // the "time is known" check is done inside the method.
        }
    }

    [PunRPC]
    void Score ( string team )
    {
        if ( team == "green" )
        {
            _greenTeamScore += 1;
        }
        if ( team == "red" )
        {   
            _redTeamScore += 1;
        }
    }

    private void StartRoundNow ()
    {
        // in some cases, when you enter a room, the server time is not available immediately.
        // time should be 0.0f but to make sure we detect it correctly, check for a very low value.
        if ( PhotonNetwork.time < 0.0001f )
        {
            // we can only start the round when the time is available. let's check that in Update()
            startRoundWhenTimeIsSynced = true;
            return;
        }
        startRoundWhenTimeIsSynced = false;

        // only use ExitGames.Client.Photon.Hashtable for Photon
        startTimeProp[StartTimeKey] = PhotonNetwork.time;

        PhotonNetwork.room.SetCustomProperties( startTimeProp );              // implement OnPhotonCustomRoomPropertiesChanged(Hashtable propertiesThatChanged) to get this change everywhere

    }

    /// <summary>Called by PUN when this client entered a room (no matter if joined or created).</summary>
    public void OnJoinedRoom ()
    {
        if ( PhotonNetwork.isMasterClient )
        {
            this.StartRoundNow();
        }
        else
        {
            Debug.Log( "StartTime already set: " + PhotonNetwork.room.customProperties.ContainsKey( StartTimeKey ) );
        }
    }

    public void OnPhotonCustomRoomPropertiesChanged ( Hashtable propertiesThatChanged )
    {
        if ( propertiesThatChanged.ContainsKey( StartTimeKey ) )
        {
            StartTime = (double)propertiesThatChanged[StartTimeKey];
        }
    }

    public void SpawnCube ()
    {
        if ( justLoaded )
        {
            GameObject goRed = (GameObject)PhotonNetwork.InstantiateSceneObject( redCube.name, redCube.transform.position, Quaternion.identity, 0, null );
            GameObject goGreen = (GameObject)PhotonNetwork.InstantiateSceneObject( greenCube.name, greenCube.transform.position, Quaternion.identity, 0, null );
            justLoaded = false;
            this.StartRoundNow();

        }
    }

    void OnGUI ()
    {
        //draw scores if in scene1
        if ( SceneManager.GetActiveScene().name == "scene1" )
        {
            GUI.Box( new Rect( Screen.width / 2, 10, 50, 20 ), "" + remainingTime.ToString( "0" ) );

            GUI.Box( new Rect( Screen.width / 4, 10, 250, 20 ), "RED TEAM: " + _redTeamScore.ToString( "0" ) );
            GUI.Box( new Rect( (Screen.width / 4) + (Screen.width / 3), 10, 250, 20 ), "GREEN TEAM: " + _greenTeamScore.ToString( "0" ) );
        }

        //playerName = GUI.TextField( new Rect( 10, 10, 100, 50 ), playerName, 25 );
    }

}
