using UnityEngine;
using System.Collections;

using Photon;

public class fire : Photon.MonoBehaviour
{

    public Rigidbody projectile;
    public Transform shotPos;
    public float shotForce = 1000f;
    public float moveSpeed = 10f;
    public float shotDistance;
    public types type;

    public Camera cam;
    //public ParticleSystem gunFire;

    public AudioSource gunSound;

    public enum types { force, forcepull, glue, Grav };

    RaycastHit hitInfo;

    float shotTimer = 0.5f;
    float nextFiretime = 0.0f;


    // Use this for initialization
    void Start ()
    {
        cam = this.GetComponentInParent<Camera>();
        gunSound = this.GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update ()
    {
        if ( photonView.isMine )
        {
            if ( Input.GetButtonUp( "Fire1" ) && Time.time > nextFiretime )
            {

                if ( type == types.force )
                {
                    ForcePush();
                }
                if ( type == types.forcepull )
                {
                    ForcePull();
                }
                if ( type == types.glue )
                {
                    GlueGun();
                }
                if ( type == types.Grav )
                {
                    GravGun();
                }
                nextFiretime = Time.time + shotTimer;
            }
        }
    }

    void ForcePush ()
    {
        Ray ray = new Ray( cam.ViewportToWorldPoint( new Vector3( 0.5f, 0.5f, cam.nearClipPlane ) ), cam.transform.forward );
        Debug.DrawRay( cam.ViewportToWorldPoint( new Vector3( 0.5f, 0.5f, cam.nearClipPlane ) ), cam.transform.forward * 50, Color.green );
        Transform hitTransform;
        Vector3 hitPoint;

        hitTransform = FindClosestHitObject( ray, out hitPoint );

        photonView.RPC( "GunEffects", PhotonTargets.All, PhotonNetwork.player.name, "Force" );
        //gunSound.Play();

        if ( hitTransform != null )
        {
            if ( hitTransform.GetComponent<PhotonView>() != null )
            {
                Debug.Log( "we hit : " + hitTransform.name );
                hitTransform.GetComponent<PhotonView>().RPC( "ForcePush", PhotonTargets.All, true, ray.direction );

                //listing who hit who on screen
                hitTransform.GetComponent<PhotonView>().RPC( "AddPlayerEvent", PhotonTargets.All, PhotonNetwork.playerName, types.force );
            }
        }
    }

    void ForcePull ()
    {
        Ray ray = new Ray( cam.ViewportToWorldPoint( new Vector3( 0.5f, 0.5f, cam.nearClipPlane ) ), cam.transform.forward );
        Debug.DrawRay( cam.ViewportToWorldPoint( new Vector3( 0.5f, 0.5f, cam.nearClipPlane ) ), cam.transform.forward * 50, Color.green );
        Transform hitTransform;
        Vector3 hitPoint;

        hitTransform = FindClosestHitObject( ray, out hitPoint );
        photonView.RPC( "GunEffects", PhotonTargets.All, PhotonNetwork.player.name, "ForcePull" );
        //gunSound.Play();

        if ( hitTransform != null )
        {
            if ( hitTransform.GetComponent<PhotonView>() != null )
            {
                Debug.Log( "we hit : " + hitTransform.name );
                hitTransform.GetComponent<PhotonView>().RPC( "ForcePull", PhotonTargets.All, true, ray.direction );

                //listing who hit who on screen
                hitTransform.GetComponent<PhotonView>().RPC( "AddPlayerEvent", PhotonTargets.All, PhotonNetwork.playerName, types.forcepull );
            }
        }
    }

    void GlueGun ()
    {
        Ray ray = new Ray( cam.ViewportToWorldPoint( new Vector3( 0.5f, 0.5f, cam.nearClipPlane ) ), cam.transform.forward );
        Debug.DrawRay( cam.ViewportToWorldPoint( new Vector3( 0.5f, 0.5f, cam.nearClipPlane ) ), cam.transform.forward * 50, Color.green );
        Transform hitTransform;
        Vector3 hitPoint;

        hitTransform = FindClosestHitObject( ray, out hitPoint );
        photonView.RPC( "GunEffects", PhotonTargets.All, PhotonNetwork.player.name, "Glue" );
        //gunSound.Play();

        if ( hitTransform != null )
        {
            if ( hitTransform.gameObject.GetComponent<RigidbodyFirstPersonController>() )
            {
                //WE HIT A PLAYER
                //get players feet position

                Debug.Log( "we hit : " + hitTransform.name );
                photonView.RPC( "glueArea", PhotonTargets.All, hitTransform.FindChild( "FeetPos" ).position );

                //we've placed glue no need to ocntinue
                return;
            }

            if ( hitTransform.name == "front" || hitTransform.name == "back" ||
                 hitTransform.name == "top" || hitTransform.name == "bottom" ||
                 hitTransform.name == "left" || hitTransform.name == "right" )
            {
                //WE HIT A WORLD BORDER
                //Don't glue this area!
                return;
            }

                Debug.Log( "we hit : " + hitTransform.name );
                photonView.RPC( "glueArea", PhotonTargets.All, hitPoint );
        }
    }

    void GravGun ()
    {
        Ray ray = new Ray( cam.ViewportToWorldPoint( new Vector3( 0.5f, 0.5f, cam.nearClipPlane ) ), cam.transform.forward );
        Debug.DrawRay( cam.ViewportToWorldPoint( new Vector3( 0.5f, 0.5f, cam.nearClipPlane ) ), cam.transform.forward * 50, Color.green );
        Transform hitTransform;
        Vector3 hitPoint;

        hitTransform = FindClosestHitObject( ray, out hitPoint );
        photonView.RPC( "GunEffects", PhotonTargets.All, PhotonNetwork.player.name, "Grav" );
        //gunSound.Play();

        if ( hitTransform != null )
        {
            if ( hitTransform.GetComponent<PhotonView>() != null )
            {
                Debug.Log( "we hit : " + hitTransform.name );
                hitTransform.GetComponent<PhotonView>().RPC( "setGravity", PhotonTargets.All );

                //listing who hit who on screen
                hitTransform.GetComponent<PhotonView>().RPC( "AddPlayerEvent", PhotonTargets.All, PhotonNetwork.playerName, types.Grav );
            }
        }
    }

    [PunRPC]
    void GunEffects ( string playerName, string gunName )
    {
        Debug.Log( playerName + " " + gunName );
        ParticleSystem GF = GameObject.Find( playerName ).transform.FindChild( "MainCamera" ).FindChild( "GunPlaceHolder" ).FindChild( gunName ).gameObject.GetComponentInChildren<ParticleSystem>();
        AudioClip GFA = GameObject.Find( playerName ).transform.FindChild( "MainCamera" ).FindChild( "GunPlaceHolder" ).FindChild( gunName ).gameObject.GetComponentInChildren<AudioSource>().clip;

        GF.Stop();
        GF.Clear();
        GF.Play();
        GF.startLifetime = GF.startLifetime;
        AudioSource.PlayClipAtPoint( GFA, GF.transform.position );
    }

    Transform FindClosestHitObject ( Ray ray, out Vector3 hitPoint )
    {

        RaycastHit[] hits = Physics.RaycastAll( ray );

        Transform closestHit = null;
        float distance = 0;
        hitPoint = Vector3.zero;

        foreach ( RaycastHit hit in hits )
        {
            if ( hit.transform != this.transform && (closestHit == null || hit.distance < distance) )
            {
                // We have hit something that is:
                // a) not us
                // b) the first thing we hit (that is not us)
                // c) or, if not b, is at least closer than the previous closest thing

                closestHit = hit.transform;
                distance = hit.distance;
                hitPoint = hit.point;
            }
        }

        // closestHit is now either still null (i.e. we hit nothing) OR it contains the closest thing that is a valid thing to hit

        return closestHit;

    }

    [PunRPC]
    void glueArea ( Vector3 pos )
    {
        PhotonNetwork.Instantiate( "GLUE", pos, Quaternion.identity, 0 );
    }

    //[PunRPC]
    /*void spawnProjectile ( Vector3 pos, Quaternion rot )
    {
            GameObject go = PhotonNetwork.Instantiate(projectile.name, pos, rot, 0);
            Rigidbody shot = go.GetComponent<Rigidbody>();
            shot.AddForce(shotPos.forward * shotForce);
            Debug.Log("SPAWNING PORJ");
    }*/
}
