﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class PlayerEvents : MonoBehaviour {

    [SerializeField]
    Text[] textRows = new Text[5];

    public Queue<string> textQueue = new Queue<string>();
    
    void start()
    {   
        for ( int i = 0; i < 5; i++ )
        {
            textQueue.Enqueue( "" );
        }
    }

    void Update ()
    {
    }

    public void updateDisplay ()
    {
        while ( textQueue.Count > 5 )
        {
            textQueue.Dequeue();
        }

        //copy queue data to tempArray then print out array using textRows
        string[] tempArray = textQueue.ToArray();
        System.Array.Reverse(tempArray);

        for ( int i = 0; i <tempArray.Length; i++ )
        {
            textRows[i].text = tempArray[i];
        }
    }

}
