﻿using UnityEngine;
using System.Collections;

public class forcePullProjectile : MonoBehaviour
{
    void Start()
    {
        Destroy(gameObject, 5.0f);

    }

    void Update()
    {

    }

    public void OnCollisionEnter(Collision collision)
    {
        //ContactPoint contact = collision.contacts[0];
        //Vector3 pos = contact.point;

        if (collision.rigidbody != null)
        {
            Vector3 forceVec = collision.rigidbody.velocity.normalized * 10.0f;
            forcePull(collision.gameObject, forceVec);
        }   

        Destroy(gameObject);
    }

    void forcePull(GameObject go, Vector3 force)
    {
        Debug.Log("PULL");

        go.GetComponent<Rigidbody>().AddForce(-force, ForceMode.Impulse);
    }
}