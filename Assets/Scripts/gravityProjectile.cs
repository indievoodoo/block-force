using UnityEngine;
using System.Collections;

public class gravityProjectile : MonoBehaviour
{
    void Start()
    {
        Destroy(gameObject, 5.0f);

    }

    void Update()
    {

    }

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.rigidbody != null)
        {
            collision.rigidbody.useGravity = false;
            collision.rigidbody.velocity += new Vector3(0, 20.0f ,0) * Time.deltaTime;
        }

        Destroy(gameObject);
    }

   

   /* IEnumerator WaitThree(GameObject go)
    {
        yield return new WaitForSeconds(3);

        go.GetComponent<Rigidbody>().useGravity = true;
        Destroy(gameObject);
    }*/
}
