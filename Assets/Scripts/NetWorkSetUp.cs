using UnityEngine;
using System.Collections;
using UnityEngine.UI;

using System.Collections.Generic;
using System.Linq;
using Photon;

public class NetWorkSetUp : Photon.MonoBehaviour
{

    public Camera cam;
    public List<GameObject> spawnspots = new List<GameObject>();
    public GameObject myPlayer;

    public int teamID;

    Vector3 mySpawnSpot = Vector3.zero;



    // Use this for initialization
    void Awake()
    {
        if (!PhotonNetwork.connected)
        {
            PhotonNetwork.ConnectUsingSettings("01");
            
        }
    }

    void Start()
    {
        Random.seed = (int)System.DateTime.Now.Ticks;
        int tempID = Random.Range(1, 3);

        //if there are more players that are green
        if (tempID == 1)
        {
           SpawnPlayer(1);
           PhotonNetwork.player.SetTeam(PunTeams.Team.red);
        }
        else
        {
            SpawnPlayer(2);
            PhotonNetwork.player.SetTeam(PunTeams.Team.green);
        }

        
    }

    void OnGUI ()
    {
        //GUILayout.Label( PhotonNetwork.connectionStateDetailed.ToString() );
    }

    void OnJoinedLobby ()
    {
        //PhotonNetwork.JoinRandomRoom();
    }

    void OnPhotonRandomJoinFailed ()
    {
        Debug.Log( "Can't join random room!" );
        PhotonNetwork.CreateRoom( null );
    }

    void SpawnPlayer(int teamId)
    {
        this.teamID = teamId;

        getTeamId();
        
        cam.enabled = false;

        PhotonNetwork.playerName = GameManager.Instance.playerName;
        myPlayer = PhotonNetwork.Instantiate( myPlayer.name, mySpawnSpot, Quaternion.identity, 0 );
        myPlayer.GetComponent<RigidbodyFirstPersonController>().enabled = true;
        myPlayer.transform.FindChild("MainCamera").gameObject.GetComponent<Camera>().enabled = true;
        myPlayer.GetComponent<PhotonView>().RPC( "setPlayerName", PhotonTargets.AllBuffered, GameManager.Instance.playerName);
        myPlayer.GetComponentInChildren<AudioListener>().enabled = true;
        myPlayer.GetComponent<PhotonView>().RPC("SetTeamId", PhotonTargets.AllBufferedViaServer, teamID);

        //myPlayer.GetComponent<PhotonView>().RPC( "IncrementTeamCount", PhotonTargets.AllBufferedViaServer, teamID );

        myPlayer.name = PhotonNetwork.playerName;
        myPlayer.transform.FindChild( "MiniMap" ).GetComponent<Camera>().enabled = true;

        GameObject.Find( "/WeaponManager" ).transform.GetComponent<WeaponManager>().enabled = true;
        GameObject.Find( "/Controller" ).transform.GetComponent<MouseController>().enabled = true;

        GameManager.Instance.loaded = true;

        if(PunTeams.PlayersPerTeam[PunTeams.Team.red].Count >= PunTeams.PlayersPerTeam[PunTeams.Team.green].Count)
        {
            PhotonNetwork.player.SetTeam(PunTeams.Team.green);
        }
        else
        {
            PhotonNetwork.player.SetTeam(PunTeams.Team.red);
        }
    }

    void getTeamId()
    {
        if (this.teamID == 1)
        {
            //red team
            mySpawnSpot = spawnspots[0].transform.position;
        }

        else
        {
            //green team
            this.teamID = 2;
            mySpawnSpot = spawnspots[1].transform.position;
        }
    }
}
