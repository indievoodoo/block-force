using UnityEngine;
using System.Collections;

public class redTeamCube : Photon.MonoBehaviour {

    public bool isDestroyed = false;
    public float respawnTimer = 5f;
    public float respawnCache;

    MeshRenderer cubeGraphics;
    BoxCollider cubeCollider;

    [PunRPC]
    void Start()
    {
         cubeGraphics = this.GetComponentInChildren<MeshRenderer>();
         cubeCollider = this.GetComponent<BoxCollider>();
         respawnCache = respawnTimer;
    }

   
	void OnTriggerExit(Collider other)
    {
        if(other.GetComponent<TeamId>().teamID == 2)
        {
            cubeGraphics.enabled = false;
            cubeCollider.enabled = false;
            isDestroyed = true;
            //GameManager.Instance.GreenTeamScore += 1;
            

            if ( other.transform.GetComponent<PhotonView>().isMine )
            {
                other.transform.GetComponent<PhotonView>().RPC( "PlayerCollectedCube", PhotonTargets.AllBufferedViaServer, PhotonNetwork.playerName, "Red" );
                //other.transform.GetComponent<PhotonView>().RPC("Score", PhotonTargets.AllBufferedViaServer, "red");
            }
            
        }
    }

    

    [PunRPC]
    void Update()
    {
        if(isDestroyed)
        {
            respawnTimer -= Time.deltaTime;
            if(respawnTimer < 0)
            {
                cubeGraphics.enabled = true;
                cubeCollider.enabled = true;
                isDestroyed = false;
                respawnTimer = respawnCache;
            }
        }
    }
}
