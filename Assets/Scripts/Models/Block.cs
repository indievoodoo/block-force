using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class Block 
{
    public enum BlockType { Air, Dirt, Grass };

    //block defaults to Air 
    private BlockType _type = BlockType.Air;

    Action<Block> cbBlockTypeChanged;
    
    //Properties
    public BlockType Type
    {
        get { return _type; }
        set
        {
            _type = value;

            //call callback to let chunk know block has changed
            if(cbBlockTypeChanged != null)
                cbBlockTypeChanged(this);
        }
    }

    // Block co-ords
    public int X { get; protected set; }
    public int Y { get; protected set; }
    public int Z { get; protected set; }

    //which chunk we're in
    public Chunk chunk;

    public Block( Chunk chunk, int x, int y, int z )
    {
        this.chunk = chunk;
        this.X = x;
        this.Y = y;
        this.Z = z;
    }

    public Block(Chunk chunk, int x, int y, int z , BlockType type)
    {
        this.chunk = chunk;
        this.X = x;
        this.Y = y;
        this.Z = z;
        this.Type = type;
    }

    public void RegisterBlockTypeChangedCallback(Action<Block> callback)
    {
        cbBlockTypeChanged += callback;
    }

    public void UnRegisterBlockTypeChangedCallback(Action<Block> callback)
    {
        cbBlockTypeChanged -= callback;
    }
}
