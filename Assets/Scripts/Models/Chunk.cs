using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class Chunk
{
    //3D array of blocks
    Block[,,] blocks;

    // Chunk co-ords
    public int X { get; protected set; }
    public int Y { get; protected set; }
    public int Z { get; protected set; }

    //chunk size (cubic)
    public int Size { get; set; }

    public Block[,,] Blocks
    {
        get
        {
            return blocks;
        }
    }

    //which chunk we're in
    World world;

    public Chunk(World world, int x, int y, int z, int size)
    {
        this.world = world;
        this.X = x;
        this.Y = y;
        this.Z = z;
        Size = size;

        blocks = new Block[Size, Size, Size];

        for (int X = 0; X < Size; X++)
        {
            for (int Y = 0; Y < Size; Y++)
            {
                for (int Z = 0; Z < Size; Z++)
                {
                    blocks[X, Y, Z] = new Block(this, x, y, z);
                }
            }
        }

        fillChunk();
    }
    
    //using Y as up/down
    private void fillChunk()
    {
        for (int x = 0; x < Size; x++) {
            for (int y = 0; y < Size; y++) {
                for (int z = 0; z < Size; z++) {
                    blocks[x, y, z].Type = Block.BlockType.Air;
                }
            }
        }
    }

    public void RandomChunk()
    {
        for (int x = 0; x < Size; x++)
        {
            for (int y = 0; y < Size; y++)
            {
                for (int z = 0; z < Size; z++)
                {
                   if(UnityEngine.Random.Range(0, 2) == 0)
                        blocks[x, y, z].Type = Block.BlockType.Air;
                   else
                        blocks[x, y, z].Type = Block.BlockType.Dirt;
                }
            }
        }
    }

    public Block getBlockAt(int x, int y, int z)
    {
        if(x < 0 || x > Size || y < 0 || y > Size || z < 0 || z > Size)
        {
            Debug.LogError("Block at (" + x + "," + y + "," + Z + ") is out of range");
            return null;
        }

        return blocks[x, y, z];
    }
}
