﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class World
{
    //3D array of blocks
    Chunk[,,] chunks;

    //chunk size (cubic)
    public int Height { get; protected set; } //Y
    public int Width { get; protected set; }  //X 
    public int Depth { get; protected set; }  //Z

    public int ChunkSize { get; set; }

    public World ( int h, int w, int d, int chunkSize )
    {
        Height = h;
        Width = w;
        Depth = d;
        ChunkSize = chunkSize;

        chunks = new Chunk[Width, Height, Depth];

        for ( int X = 0; X < Width; X++ )
        {
            for ( int Y = 0; Y < Height; Y++ )
            {
                for ( int Z = 0; Z < Depth; Z++ )
                {
                    chunks[X, Y, Z] = new Chunk( this, X * ChunkSize, Y * ChunkSize, Z * ChunkSize, ChunkSize );
                }
            }
        }
    }

    public Chunk getChunkAt ( int x, int y, int z )
    {
        return chunks[x, y, z];
    }
}

