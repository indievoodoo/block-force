using UnityEngine;
using System.Collections;

public class forcePushProjectile : MonoBehaviour
{
    //float fraction = 0;
    public float forceMult = 10.0f;

    void Start()
    {
        Destroy(gameObject, 5.0f);

    }

    void Update()
    {
        
    }

    public void OnCollisionEnter(Collision collision)
    {
        Vector3 ForceVec = this.GetComponent<Rigidbody>().velocity * forceMult ;
        
        if (collision.rigidbody != null)
        {
            GameObject go = collision.gameObject;
            if(go == null)
            {
                Debug.Log("go is null");
            }
            
            go.GetComponent<RigidbodyFirstPersonController>().forceVec = ForceVec;
            go.GetComponent<RigidbodyFirstPersonController>().forceHit = true;
        }
        
        PhotonNetwork.Destroy(gameObject);
    }

    /*void forcePush(GameObject go, Vector3 force)
    {
        Debug.Log("PUSH");

        fraction = fraction + Time.deltaTime * 9;

        go.GetComponent<Rigidbody>().velocity += force;
        go.transform.position += Vector3.Lerp(go.transform.position, (go.transform.position + force), fraction);
    }*/
}
