﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour
{
    [SerializeField]
    GameObject GLUE;

    void Start()
    {
        Destroy(gameObject, 5.0f);

    }

    void Update()
    {

    }

    public void OnCollisionEnter(Collision collision)
    {
        //ContactPoint contact = collision.contacts[0];
        //Vector3 pos = contact.point;
        
        Destroy(gameObject);
    }

    void glueArea(Vector3 pos)
    {
        Instantiate(GLUE, pos, Quaternion.identity);
    }

    void forcePush(GameObject go)
    {

    }
}
