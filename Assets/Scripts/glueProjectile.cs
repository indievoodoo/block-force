using UnityEngine;
using System.Collections;

public class glueProjectile : Photon.MonoBehaviour
{
    [SerializeField]
    GameObject GLUE;

    void Start ()
    {
        Destroy( gameObject, 5.0f );

    }

    void Update ()
    {

    }

    public void OnCollisionEnter ( Collision collision )
    {
        ContactPoint contact = collision.contacts[0];
        Debug.Log( contact.otherCollider.gameObject.name );
        Vector3 pos = contact.point;

        glueArea( pos );

        Debug.Log( pos );
        if ( contact.thisCollider.name != "GLUE(clone)" )
        {
            photonView.RPC( "glueArea", PhotonTargets.AllViaServer, pos );
        }

        Destroy( gameObject );
    }

    [PunRPC]
    void glueArea ( Vector3 pos )
    {
        /*Vector3 temp = new Vector3(
            Mathf.Ceil( pos.x ) - .5f,
            Mathf.Ceil( pos.y ) + 0.05f,
            Mathf.Ceil( pos.z ) - .5f
        );*/

        //Instantiate( GLUE, pos, Quaternion.identity );
        PhotonNetwork.Instantiate( "GLUE", pos, Quaternion.identity, 0 );
    }

}
