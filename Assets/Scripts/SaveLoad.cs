﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class SaveLoad : MonoBehaviour
{
    public static List<World> savedWorld = new List<World>();
    public WorldController wc;
    public World localCopyOfWorld;

    //it's static so we can call it from anywhere
    public void Save ()
    {
        if ( !Directory.Exists( Application.dataPath.ToString() + "/Resources/Saves" ) )
            Directory.CreateDirectory( Application.dataPath.ToString() + "/Resources/Saves" );
        if ( !Directory.Exists( Application.dataPath.ToString() + "/Resources/Saves/TESTING" ) )
            Directory.CreateDirectory( Application.dataPath.ToString() + "/Resources/Saves/TESTING" );

        StreamWriter dataTXT = new StreamWriter( Application.dataPath.ToString() + "/Resources/Saves/TESTING/data.txt" );

        wc = WorldController.Instance;
        dataTXT.WriteLine( wc.chunkSize );
        dataTXT.WriteLine( wc.height );
        dataTXT.WriteLine( wc.width );
        dataTXT.WriteLine( wc.depth );

        dataTXT.Close();

        //create CHUNK files
        for ( int x = 0; x < wc.width; x++ )
        {
            for ( int y = 0; y < wc.height; y++ )
            {
                for ( int z = 0; z < wc.depth; z++ )
                {
                    StreamWriter chunkTEMP = new StreamWriter( Application.dataPath.ToString() + "/Resources/Saves/TESTING/Chunk_" + x + "_ " + y + "_" + z + ".txt" );

                    Chunk tempChunk = wc.World.getChunkAt( x, y, z );

                    Block[,,] tempBlock = tempChunk.Blocks;

                    //tempBlock[i, j, k];
                    for ( int i = 0; i < wc.chunkSize; i++ )
                    {
                        for ( int j = 0; j < wc.chunkSize; j++ )
                        {
                            for ( int k = 0; k < wc.chunkSize; k++ )
                            {
                                string result = "";
                                result = GetString( tempBlock[i, j, k].Type );

                                chunkTEMP.Write( result );
                            }
                            chunkTEMP.WriteLine();
                        }
                        chunkTEMP.WriteLine();
                    }

                    chunkTEMP.Close();
                }
            }
        }

        Debug.Log( "SAVED" );
    }

    string GetString(Block.BlockType bt)
    {
        switch ( bt )
        {
            case Block.BlockType.Dirt:
                return "1";
            case Block.BlockType.Air:
                return "0";
        }

        Debug.Log( "NULLAGE" );
        return "";
    }

    public void Load ()
    {
        StreamReader dataTXT = new StreamReader( Application.dataPath.ToString() + "/Resources/Saves/TESTING/data.txt" );

        wc = WorldController.Instance;
        wc.chunkSize = int.Parse( dataTXT.ReadLine() );
        wc.height = int.Parse( dataTXT.ReadLine() );
        wc.width = int.Parse( dataTXT.ReadLine() );
        wc.depth = int.Parse( dataTXT.ReadLine() );

        dataTXT.Close();

        //create CHUNK files
        for ( int x = 0; x < wc.width; x++ )
        {
            for ( int y = 0; y < wc.height; y++ )
            {
                for ( int z = 0; z < wc.depth; z++ )
                {
                    StreamReader chunkTEMP = new StreamReader( Application.dataPath.ToString() + "/Resources/Saves/TESTING/Chunk_" + x + "_ " + y + "_" + z + ".txt" );

                    Chunk tempChunk = wc.World.getChunkAt( x, y, z );

                    Block[,,] tempBlock = tempChunk.Blocks;

                    //tempBlock[i, j, k];
                    for ( int i = 0; i < wc.chunkSize; i++ )
                    {
                        for ( int j = 0; j < wc.chunkSize; j++ )
                        {
                            for ( int k = 0; k < wc.chunkSize; k++ )
                            {
                                int result;
                                result = chunkTEMP.Read();
                                tempBlock[i, j, k].Type = GetType( result );
                            }
                            chunkTEMP.ReadLine();
                        }
                        chunkTEMP.ReadLine();
                    }

                    chunkTEMP.Close();
                }
            }
        }

        Debug.Log( "LOADED" );
    }

    Block.BlockType GetType( int bt )
    {
        switch (bt)
        {
            case 0:
                return Block.BlockType.Air;
            case 1:
                return Block.BlockType.Dirt;
        }
        return Block.BlockType.Air;
    }
}