using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;

[Serializable]
public class WorldController : MonoBehaviour
{
    public List<Block> blockToSend = new List<Block>();

    public PhysicMaterial newMat;

    public GameObject controller;

    public GameObject leftBoxCollider, rightBoxCollider, frontBoxCollider, backBoxCollider, topBoxCollider, bottomBoxCollider;

    public static WorldController Instance { get; protected set; }

    //public PhysicMaterial zeroMat;

    public World World
    {
        get
        {
            return world;
        }

        set
        {
            world = value;
        }
    }

    //only one block dirt so this for now.
    [SerializeField]
    Material dirt;

    [Header( "Size In Chunks" )]
    [SerializeField]
    public int height;
    [SerializeField]
    public int width;
    [SerializeField]
    public int depth;

    [Header( "Chunk Size" )]
    [SerializeField]
    public int chunkSize;

    //the world with chunks and then blocks ;)
    World world;

    // Use this for initialization
    void Start ()
    {


        //UnityEngine.Random.seed = 546548546;
        //Debug.Log( UnityEngine.Random.seed );


        if ( Instance != null )
        {
            Debug.LogError( "There shoudl only be one World Controller in the scene!" );
        }
        Instance = this;

        LoadData();

        //init world
        World = new World( height, width, depth, chunkSize );

        //calcualte terrain
        //calcTerrain();
        Load();

        GameObject world_go = new GameObject( "World" );

        //create game object for each chunk in world
        for ( int i = 0; i < width; i++ )
        {
            for ( int j = 0; j < height; j++ )
            {
                for ( int k = 0; k < depth; k++ )
                {
                    Chunk chunk_data = World.getChunkAt( i, j, k );

                    //create game object for chunk
                    GameObject chunk_go = new GameObject();
                    chunk_go.transform.SetParent( world_go.transform );
                    chunk_go.name = "Chunk_" + i + "_" + j + "_" + k;
                    chunk_go.transform.position = new Vector3( chunk_data.X, chunk_data.Y, chunk_data.Z );

                    chunk_data = World.getChunkAt( i, j, k );

                    Mesh reducedMesh = ReduceMesh( chunk_data );

                    MeshFilter chunk_mf = chunk_go.AddComponent<MeshFilter>();
                    chunk_mf.mesh = reducedMesh;

                    MeshRenderer chunk_mr = chunk_go.AddComponent<MeshRenderer>();
                    chunk_mr.material = dirt;

                    MeshCollider chunk_mc = chunk_go.AddComponent<MeshCollider>();

                    //this stops us getting stuck have changed some othert stuff too!
                    //BoxCollider box = chunk_go.AddComponent<BoxCollider>();


                    leftBoxCollider.GetComponent<BoxCollider>().size = new Vector3( 1, chunkSize*height, width * chunkSize +1);
                    leftBoxCollider.transform.position = new Vector3( -1, ( chunkSize* height)/2, (chunkSize * depth) / 2 );

                    rightBoxCollider.GetComponent<BoxCollider>().size = new Vector3( 1, chunkSize * height, width * chunkSize +1 );
                    rightBoxCollider.transform.position = new Vector3( (chunkSize * width) + 1, (chunkSize * height) / 2, (chunkSize * depth) / 2 );

                    frontBoxCollider.GetComponent<BoxCollider>().size = new Vector3( 1, chunkSize * height, width * chunkSize +1 );
                    frontBoxCollider.transform.position = new Vector3( (width * chunkSize) / 2, (chunkSize * height) / 2, -1 );

                    backBoxCollider.GetComponent<BoxCollider>().size = new Vector3( 1, chunkSize * height, width * chunkSize +1 );
                    backBoxCollider.transform.position = new Vector3( (width * chunkSize) / 2, (chunkSize * height) / 2, width * chunkSize + 1 );

                    bottomBoxCollider.GetComponent<BoxCollider>().size = new Vector3( 1, width * chunkSize, width * chunkSize );
                    bottomBoxCollider.transform.position = new Vector3( (width * chunkSize) / 2, 0, (width * chunkSize) / 2 );

                    topBoxCollider.GetComponent<BoxCollider>().size = new Vector3( 1, width * chunkSize + 1, width * chunkSize + 1 );
                    topBoxCollider.transform.position = new Vector3( (width * chunkSize) / 2, chunkSize * height, (width * chunkSize) / 2 );
                    



                    /*worldBoxCollider = controller.AddComponent<BoxCollider>();
                    worldBoxCollider.transform.position = controller.transform.position;
                    worldBoxCollider.size = new Vector3((width * chunkSize), (height * chunkSize), (depth * chunkSize));*/

                    if ( newMat != null )
                    {
                        chunk_mc.material = newMat;
                    }
                    else
                    {
                        //Debug.Log("newmat is null");
                    }



                    for ( int x = 0; x < chunkSize; x++ )
                    {
                        for ( int y = 0; y < chunkSize; y++ )
                        {
                            for ( int z = 0; z < chunkSize; z++ )
                            {
                                Block block_data = chunk_data.getBlockAt( x, y, z );

                                block_data.RegisterBlockTypeChangedCallback( ( block ) => { onBlockChanged( chunk_data, block, chunk_go ); } );
                            }
                        }
                    }
                }
            }
        }
    }

    void calcTerrain ()
    {
        for ( int x = 0; x < chunkSize * width; x++ )
        {
            for ( int z = 0; z < chunkSize * depth; z++ )
            {
                float noiseVal = Noise( x, z, 65f, 0.8f, 0.6f );

                for ( int y = 0; y < (int)((chunkSize * world.Height) * Mathf.Clamp( noiseVal, 0.0f, 1.0f )); y++ )
                {
                    world.getChunkAt( (int)Mathf.Floor( (x) / chunkSize ), (int)Mathf.Floor( (y) / chunkSize ), (int)Mathf.Floor( z / chunkSize ) ).getBlockAt( x % chunkSize, y % chunkSize, z % chunkSize ).Type = Block.BlockType.Dirt;
                }
            }
        }
    }

    private float Noise ( int x, int y, float scale, float mag, float exp )
    {

        return (Mathf.Pow( (Mathf.PerlinNoise( (x + 500) / scale, (y + 500) / scale ) * mag), (exp) ));

        //return Mathf.PerlinNoise( x / scale, y / scale ) * mag;

    }

    public void onBlockChanged ( Chunk chunk_data, Block block_data, GameObject chunk_go )
    {
        //recalc chunk mesh
        Mesh reducedMesh = ReduceMesh( chunk_data );

        //appply new mesh to game object
        MeshFilter chunk_mf = chunk_go.GetComponent<MeshFilter>();
        chunk_mf.mesh = reducedMesh;

        //upate mesh collider
        MeshCollider chunk_mc = chunk_go.GetComponent<MeshCollider>();
        chunk_mc.sharedMesh = reducedMesh;

        Debug.Log( "redraw chunks!" );
    }



    // Update is called once per frame
    void Update ()
    {

    }

    // The MIT License (MIT)
    //
    // Copyright (c) 2012-2013 Mikola Lysenko
    //
    // Permission is hereby granted, free of charge, to any person obtaining a copy
    // of this software and associated documentation files (the "Software"), to deal
    // in the Software without restriction, including without limitation the rights
    // to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    // copies of the Software, and to permit persons to whom the Software is
    // furnished to do so, subject to the following conditions:
    //
    // The above copyright notice and this permission notice shall be included in
    // all copies or substantial portions of the Software.
    //
    // THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    // IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    // FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    // AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    // LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    // OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    // THE SOFTWARE.

    public static Mesh ReduceMesh ( Chunk chunk )
    {
        List<Vector3> vertices = new List<Vector3>();
        List<int> elements = new List<int>();
        List<Vector2> uvs = new List<Vector2>();
        //List<Vector3> colours = new List<Vector3>();

        int size = chunk.Size;

        //Sweep over 3-axes
        for ( int d = 0; d < 3; d++ )
        {

            int i, j, k, l, w, h, u = (d + 1) % 3, v = (d + 2) % 3;

            int[] x = new int[3];
            int[] q = new int[3];
            int[] mask = new int[(size + 1) * (size + 1)];

            q[d] = 1;

            for ( x[d] = -1; x[d] < size; )
            {

                // Compute the mask
                int n = 0;
                for ( x[v] = 0; x[v] < size; ++x[v] )
                {
                    for ( x[u] = 0; x[u] < size; ++x[u], ++n )
                    {


                        int a = 0;
                        if ( 0 <= x[d] )
                        {
                            a = (int)chunk.getBlockAt( x[0], x[1], x[2] ).Type;

                            //if (noCollision.IndexOf(a) != -1){a = 0;}
                        }

                        int b = 0;
                        if ( x[d] < size - 1 )
                        {
                            b = (int)chunk.getBlockAt( x[0] + q[0], x[1] + q[1], x[2] + q[2] ).Type;

                            //if (noCollision.IndexOf(b) != -1) {b = 0;}
                        }

                        if ( a != -1 && b != -1 && a == b )
                        {
                            mask[n] = 0;
                        }
                        else if ( a > 0 )
                        {
                            a = 1;
                            mask[n] = a;
                        }

                        else
                        {
                            b = 1;
                            mask[n] = -b;
                        }

                    }


                }

                // Increment x[d]
                ++x[d];

                // Generate mesh for mask using lexicographic ordering
                n = 0;
                for ( j = 0; j < size; ++j )
                {
                    for ( i = 0; i < size; )
                    {
                        var c = mask[n]; if ( c > -3 )
                        {
                            // Compute width
                            for ( w = 1; c == mask[n + w] && i + w < size; ++w ) { }

                            // Compute height
                            bool done = false;
                            for ( h = 1; j + h < size; ++h )
                            {
                                for ( k = 0; k < w; ++k )
                                {
                                    if ( c != mask[n + k + h * size] )
                                    {
                                        done = true; break;
                                    }
                                }

                                if ( done ) break;
                            }

                            // Add quad 
                            bool flip = false;
                            x[u] = i;
                            x[v] = j;

                            int[] du = new int[3];
                            int[] dv = new int[3];

                            if ( c > -1 )
                            {
                                du[u] = w;
                                dv[v] = h;
                            }
                            else
                            {
                                flip = true;
                                c = -c;
                                du[u] = w;
                                dv[v] = h;
                            }


                            Vector3 v1 = new Vector3( x[0], x[1], x[2] );
                            Vector3 v2 = new Vector3( x[0] + du[0], x[1] + du[1], x[2] + du[2] );
                            Vector3 v3 = new Vector3( x[0] + du[0] + dv[0], x[1] + du[1] + dv[1], x[2] + du[2] + dv[2] );
                            Vector3 v4 = new Vector3( x[0] + dv[0], x[1] + dv[1], x[2] + dv[2] );

                            if ( c > 0 && !flip )
                            {
                                AddFace( v1, v2, v3, v4, vertices, elements, uvs, 0 );
                            }

                            if ( flip )
                            {
                                AddFace( v4, v3, v2, v1, vertices, elements, uvs, 0 );
                            }

                            // Zero-out mask
                            for ( l = 0; l < h; ++l )
                                for ( k = 0; k < w; ++k )
                                {
                                    mask[n + k + l * size] = 0;
                                }

                            // Increment counters and continue
                            i += w; n += w;
                        }

                        else
                        {
                            ++i;
                            ++n;
                        }
                    }
                }
            }
        }

        Mesh mesh = new Mesh();
        mesh.Clear();
        mesh.vertices = vertices.ToArray();
        mesh.triangles = elements.ToArray();
        mesh.uv = uvs.ToArray();
        mesh.Optimize();
        mesh.RecalculateBounds();
        mesh.RecalculateNormals();

        return mesh;
    }

    private static void AddFace ( Vector3 v1, Vector3 v2, Vector3 v3, Vector3 v4, List<Vector3> vertices, List<int> elements, List<Vector2> uvs, int order )
    {
        calculateUVs( v1, v2, v3, v4, uvs );

        if ( order == 0 )
        {
            int index = vertices.Count;

            vertices.Add( v1 );
            vertices.Add( v2 );
            vertices.Add( v3 );
            vertices.Add( v4 );

            elements.Add( index );
            elements.Add( index + 1 );
            elements.Add( index + 2 );
            elements.Add( index + 2 );
            elements.Add( index + 3 );
            elements.Add( index );
        }

        if ( order == 1 )
        {
            int index = vertices.Count;

            vertices.Add( v1 );
            vertices.Add( v2 );
            vertices.Add( v3 );
            vertices.Add( v4 );

            elements.Add( index );
            elements.Add( index + 3 );
            elements.Add( index + 2 );
            elements.Add( index + 2 );
            elements.Add( index + 1 );
            elements.Add( index );
        }
    }

    private static void calculateUVs ( Vector3 v1, Vector3 v2, Vector3 v3, Vector3 v4, List<Vector2> uvs )
    {
        if ( v1.z == v2.z && v3.z == v4.z && v1.z == v4.z )
        {
            uvs.Add( new Vector2( v1.x, v1.y ) );
            uvs.Add( new Vector2( v2.x, v2.y ) );
            uvs.Add( new Vector2( v3.x, v3.y ) );
            uvs.Add( new Vector2( v4.x, v4.y ) );
        }

        if ( v1.y == v2.y && v3.y == v4.y && v1.y == v4.y )
        {
            uvs.Add( new Vector2( v1.x, v1.z ) );
            uvs.Add( new Vector2( v2.x, v2.z ) );
            uvs.Add( new Vector2( v3.x, v3.z ) );
            uvs.Add( new Vector2( v4.x, v4.z ) );
        }

        if ( v1.x == v2.x && v3.x == v4.x && v1.x == v4.x )
        {
            uvs.Add( new Vector2( v1.y, v1.z ) );
            uvs.Add( new Vector2( v2.y, v2.z ) );
            uvs.Add( new Vector2( v3.y, v3.z ) );
            uvs.Add( new Vector2( v4.y, v4.z ) );
        }
    }

    public void LoadData ()
    {

        //FileInfo theSourceFile = new FileInfo( Application.dataPath.ToString() + "/Resources/Saves/TESTING/data.txt" );
        TextAsset TA = Resources.Load( "Saves/TESTING/data" ) as TextAsset;
        if ( TA == null ) Debug.Log( "TA null" );

        string[] dataLines = TA.text.Split( '\n' );

        //StreamReader dataTXT = theSourceFile.OpenText();
        //StreamReader dataTXT = TA;


        chunkSize = int.Parse( dataLines[0] ); //int.Parse( dataTXT.ReadLine() );
        height = int.Parse( dataLines[1] ); // int.Parse( dataTXT.ReadLine() );
        width = int.Parse( dataLines[2] ); //int.Parse( dataTXT.ReadLine() );
        depth = int.Parse( dataLines[3] ); //int.Parse( dataTXT.ReadLine() );

        //dataTXT.Close();
        //Debug.Log( "data loaded" );
    }

    void Load ()
    {
        for ( int x = 0; x < width; x++ )
        {
            for ( int y = 0; y < height; y++ )
            {
                for ( int z = 0; z < depth; z++ )
                {
                    //FileInfo tsf = new FileInfo( Application.dataPath.ToString() + "/Resources/Saves/TESTING/Chunk_" + x + "_ " + y + "_" + z + ".txt" );
                    //StreamReader chunkTEMP = tsf.OpenText();

                    string filename = "Saves/TESTING/Chunk_" + x + "_ " + y + "_" + z;

                    TextAsset chunkTA = Resources.Load( filename ) as TextAsset;
                    if ( chunkTA == null ) Debug.Log( "null TA" );
                    int lineCount = 0;

                    for ( int i = 0; i < chunkSize; i++ )
                    {
                        String[] chunkLines = chunkTA.text.Split( '\n' );

                        for ( int j = 0; j < chunkSize; j++ )
                        {
                            //string text = chunkTEMP.ReadLine();
                            //char[] bits = text.ToCharArray();

                            char[] bits = chunkLines[lineCount].ToCharArray();
                            lineCount++;

                            for ( int k = 0; k < chunkSize; k++ )
                            {
                                ////result = int.Parse( chunkTEMP.Read() );
                                //if( j == 1 )
                                //{
                                //    Debug.Log( bits[k] );
                                //}
                                switch ( bits[k] )
                                {
                                    case '0':
                                        world.getChunkAt( x, y, z ).getBlockAt( i, j, k ).Type = Block.BlockType.Air;
                                        break;
                                    case '1':
                                        world.getChunkAt( x, y, z ).getBlockAt( i, j, k ).Type = Block.BlockType.Dirt;
                                        break;
                                }
                            }
                            //chunkTEMP.ReadLine();
                        }
                        //chunkTEMP.ReadLine();
                        lineCount++;
                    }
                }
            }
        }

        //Debug.Log( "LOADED" );
    }
}
