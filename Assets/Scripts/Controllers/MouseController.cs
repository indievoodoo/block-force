using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Photon;

public class MouseController : Photon.MonoBehaviour
{
    public List<Block> blockToSend = new List<Block>();
    World world;
    Camera cam;


    void Start()
    {
        world = WorldController.Instance.World;
          
    }

    void Onenable()
    {
        cam = GameObject.Find( PhotonNetwork.playerName ).transform.FindChild( "MainCamera" ).GetComponent<Camera>();
    }

    void Update()
    {
        world = WorldController.Instance.World;
        //Ray pickRay = cam.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0f));
        
        if(cam == null)
        {
            cam = GameObject.Find( PhotonNetwork.playerName ).transform.FindChild( "MainCamera" ).GetComponent<Camera>();
        }

        // if left mouse clicked remove block
        if (Input.GetMouseButtonUp(0))
        {
            Ray pickRay = new Ray(cam.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, cam.nearClipPlane)), cam.transform.forward);

            Debug.DrawRay(cam.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, cam.nearClipPlane)), cam.transform.forward * 50);
            RaycastHit hit;

            if (Physics.Raycast(pickRay, out hit))
            {
                Vector3 hitPoint = hit.point;

                //find out which direction to place block in
                if (hit.point.x - (int)hit.point.x == 0)
                {
                    if (hit.point.x > cam.transform.position.x)
                    {
                        //negative
                        //Debug.Log("neg x");
                    }
                    else if (hit.point.x < cam.transform.position.x)
                    {
                        //positive
                        //Debug.Log("pos x");
                        //add one to x
                        hitPoint.x -= 1;
                    }
                }
                else if (hit.point.y - (int)hit.point.y == 0)
                {
                    if (hit.point.y > cam.transform.position.y)
                    {
                        //negative
                        //Debug.Log("neg y");
                    }
                    else if (hit.point.y < cam.transform.position.y)
                    {
                        //positive
                        //Debug.Log("pos y");
                        //add one to y
                        hitPoint.y -= 1;
                    }
                }
                else if (hit.point.z - (int)hit.point.z == 0)
                {
                    if (hit.point.z > cam.transform.position.z)
                    {
                        //negative
                        //Debug.Log("neg z");
                    }
                    else if (hit.point.z < cam.transform.position.z)
                    {
                        //positive
                        //Debug.Log("pos z");
                        //add one to z
                        hitPoint.z -= 1;
                    }
                }


                //determine chunk rayhit is in
                Vector3 hitChunk = new Vector3(Mathf.Floor(hitPoint.x / 16), Mathf.Floor(hitPoint.y / 16), Mathf.Floor(hitPoint.z / 16));

                //determine block rayhit is in
                Vector3 hitBlock = new Vector3(Mathf.Floor(hitPoint.x % 16), Mathf.Floor(hitPoint.y % 16), Mathf.Floor(hitPoint.z % 16));
                //Block b = world.getChunkAt((int)hitChunk.x, (int)hitChunk.y, (int)hitChunk.z).getBlockAt((int)hitBlock.x, (int)hitBlock.y, (int)hitBlock.z);
                //Debug.Log(hit.point);
                //Debug.Log(hitPoint);

                //set type of block
                Vector3 chunk = new Vector3(hitChunk.x, hitChunk.y, hitChunk.z);
                Vector3 block = new Vector3(hitBlock.x, hitBlock.y, hitBlock.z);
                this.GetComponent<PhotonView>().RPC("blockTypeAir", PhotonTargets.AllBufferedViaServer, chunk , block);
                blockTypeAir(chunk, block);
                
                
                //b.Type = Block.BlockType.Air;
                /*Chunk chunkToSend = world.getChunkAt((int)hitChunk.x, (int)hitChunk.y, (int)hitChunk.z);
                blockToSend.Add(new Block(chunkToSend, (int)hitBlock.x, (int)hitBlock.y, (int)hitBlock.z, Block.BlockType.Air));*/
                
            }
        }

        if (Input.GetMouseButtonUp(1))
        {
            if ( cam == null ) Debug.Log( "CAM NULL" );

            Ray pickRay = new Ray(cam.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, cam.nearClipPlane)), cam.transform.forward);

            Debug.DrawRay(cam.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, cam.nearClipPlane)), cam.transform.forward * 50);
            RaycastHit hit;
            
            if (Physics.Raycast(pickRay, out hit))
            {
                Vector3 hitPoint = hit.point;

                //find out which direction to place block in
                if (hit.point.x - (int)hit.point.x == 0)
                {
                    if (hit.point.x > cam.transform.position.x)
                    {
                        //negative
                        //Debug.Log("neg x");
                        //take one of x axis
                        hitPoint.x -= 1;
                    }
                    else if (hit.point.x < cam.transform.position.x)
                    {
                        //positive
                        //Debug.Log("pos x");
                    }
                }
                else if (hit.point.y - (int)hit.point.y == 0)
                {
                    if (hit.point.y > cam.transform.position.y)
                    {
                        //negative
                        //Debug.Log("neg y");
                        //take one of y axis
                        hitPoint.y -= 1;
                    }
                    else if (hit.point.y < cam.transform.position.y)
                    {
                        //positive
                        //Debug.Log("pos y");
                    }
                }
                else if (hit.point.z - (int)hit.point.z == 0)
                {
                    if (hit.point.z > cam.transform.position.z)
                    {
                        //negative
                        //Debug.Log("neg z");
                        //take one of z axis
                        hitPoint.z -= 1;
                    }
                    else if (hit.point.z < cam.transform.position.z)
                    {
                        //positive
                        //Debug.Log("pos z");
                    }
                }


                //determine chunk rayhit is in
                Vector3 hitChunk = new Vector3(Mathf.Floor(hitPoint.x / 16), Mathf.Floor(hitPoint.y / 16), Mathf.Floor(hitPoint.z / 16));

                //determine block rayhit is in
                Vector3 hitBlock = new Vector3(Mathf.Floor(hitPoint.x % 16), Mathf.Floor(hitPoint.y % 16), Mathf.Floor(hitPoint.z % 16));
                //Block b = world.getChunkAt((int)hitChunk.x, (int)hitChunk.y, (int)hitChunk.z).getBlockAt((int)hitBlock.x, (int)hitBlock.y, (int)hitBlock.z);
                //Debug.Log(hit.point);
                //Debug.Log(hitPoint);

                //set type of block
                Vector3 chunk = new Vector3(hitChunk.x, hitChunk.y, hitChunk.z);
                Vector3 block = new Vector3(hitBlock.x, hitBlock.y, hitBlock.z);
                this.GetComponent<PhotonView>().RPC("blockTypeDirt", PhotonTargets.AllBufferedViaServer , chunk , block );
                blockTypeDirt(chunk , block);
                
                //b.Type = Block.BlockType.Dirt;
                /*Chunk chunkToSend = world.getChunkAt((int)hitChunk.x, (int)hitChunk.y, (int)hitChunk.z);
                blockToSend.Add(new Block(chunkToSend , (int)hitBlock.x , (int)hitBlock.y , (int)hitBlock.z , Block.BlockType.Dirt));*/
            }
        }
    }

    [PunRPC]
    void blockTypeAir(Vector3 chunk , Vector3 block)
    {
        Block b = world.getChunkAt((int)chunk.x, (int)chunk.y, (int)chunk.z).getBlockAt((int)block.x, (int)block.y, (int)block.z);
        b.Type = Block.BlockType.Air;
    }

    [PunRPC]
    void blockTypeDirt(Vector3 chunk , Vector3 block)
    {
        Block b = world.getChunkAt((int)chunk.x, (int)chunk.y, (int)chunk.z).getBlockAt((int)block.x, (int)block.y, (int)block.z);
        b.Type = Block.BlockType.Dirt;
    }

    
    /*private static byte[] SerializeBlock(object customObject)
    {
        Block block = (Block)customObject;

        byte[] bytes = 
    }*/

    void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        #region
        /*
        if (stream.isWriting)
        {
            //Send block.
            if (blockToSend.Count == 0)
            {
                Debug.Log("blockToSend List is empty");
            }
            if (blockToSend.Count > 0)
            {
                Debug.Log("sending block");
                
                stream.SendNext(blockToSend[0].chunk.X);
                stream.SendNext(blockToSend[0].chunk.Y);
                stream.SendNext(blockToSend[0].chunk.Z);
                stream.SendNext(blockToSend[0].X);
                stream.SendNext(blockToSend[0].Y);
                stream.SendNext(blockToSend[0].Z);
                stream.SendNext(blockToSend[0].Type);
                blockToSend.Remove(blockToSend[0]);
                Debug.Log("removed block");
            }
        }
        else if (stream.isReading)
        {
            int chunkX = (int)stream.ReceiveNext();
            int chunkY = (int)stream.ReceiveNext();
            int chunkZ = (int)stream.ReceiveNext();

            int blockX = (int)stream.ReceiveNext();
            int blockY = (int)stream.ReceiveNext();
            int blockZ = (int)stream.ReceiveNext();

            int type = (int)stream.ReceiveNext();

            Debug.Log("chunk X " + (chunkX/16).ToString() + "chunk Y " + (chunkY/16).ToString() + "chunk Z " + (chunkZ/16).ToString());
            Debug.Log("block X" + (blockX%16).ToString() + "block Y" + (blockY%16).ToString() + "block Z" + (blockZ%16).ToString());
            Debug.Log(type.ToString());

            // recive block. 
            Block br;

            br = world.getChunkAt((chunkX / 16), (chunkY / 16), (chunkZ / 16)).getBlockAt((blockX % 16), (blockY % 16), (blockZ % 16));
            br.Type = (Block.BlockType)type;
        }
         */
        #endregion

        #region
        /*if (stream.isWriting)
        {
            stream.SendNext(blockToSend[0].chunk.X);
            stream.SendNext(blockToSend[0].chunk.Y);
            stream.SendNext(blockToSend[0].chunk.Z);

            stream.SendNext(blockToSend[0].X);
            stream.SendNext(blockToSend[0].Y);
            stream.SendNext(blockToSend[0].Z);

            stream.SendNext(blockToSend[0].Type);
            blockToSend.RemoveAt(0);
        }
        else
        {
            int chunkX = (int)stream.ReceiveNext();
            int chunky = (int)stream.ReceiveNext();
            int chunkz = (int)stream.ReceiveNext();
            Debug.Log("CX " + chunkX.ToString() + " CY " + chunky.ToString() + " CZ " + chunkz.ToString());

            int blockX = (int)stream.ReceiveNext();
            int blocky = (int)stream.ReceiveNext();
            int blockz = (int)stream.ReceiveNext();
            Debug.Log("BX " + blockX.ToString() + " BY " + blocky.ToString() + " BZ " + blockz.ToString());

            int type = (int)stream.ReceiveNext();

            Block blockToChange = world.getChunkAt(chunkX, chunky, chunkz).getBlockAt(blockX, blocky, blockz);
            blockToChange.Type = (Block.BlockType)type;
        }*/
        #endregion
    }
}
