using UnityEngine;
using System.Collections;

public class TeamId : MonoBehaviour {

    public int _teamID = 0;

    //SkinnedMeshRenderer mySkin;
    public GameObject cylinder;
    public MeshRenderer cynSkin;

    public int teamID
    {
        get { return _teamID;  }
    }

    void start()
    {
        //mySkin = this.transform.GetComponentInChildren<SkinnedMeshRenderer>();
    }

    public int GetTeamID()
    {
        return _teamID;
    }

    [PunRPC]
    void SetTeamId(int id)
    {
        _teamID = id;

        SkinnedMeshRenderer mySkin = this.transform.GetComponentInChildren<SkinnedMeshRenderer>();
        

        if (mySkin == null)
        {
            Debug.LogError("Couldn't find a SkinnedMeshRenderer!");
        }

        if (_teamID == 1)
        {
            mySkin.material.color = Color.red;
            cynSkin.material.color = Color.red;
        }

        if (_teamID == 2)
        {
            mySkin.material.color = new Color(.5f, 1f, .5f);
            cynSkin.material.color = new Color(.5f, 1f, .5f);
        }
    }
    
    /*
     * This method has to be in this script because it's the only script atatched to the rot player with easy access.
     * and the setName RPC has to be called by the player setting their own name.
     * In reality this should be in it's own "PlayerDetails" script maybe.
     */
    [PunRPC]
    void setPlayerName ( string pName )
    {
        GameObject.Find( pName ).GetComponentInChildren<TextMesh>().text = pName;
    }
}
