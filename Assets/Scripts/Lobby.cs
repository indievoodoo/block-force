﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Lobby : MonoBehaviour
{

    [SerializeField]
    string version = "1.0a";

    [SerializeField]
    GameObject roomItem;
    [SerializeField]
    RectTransform container;

    bool joined = false;
    bool listChanged = false;

    RoomInfo[] roomInfo;

    void Start ()
    {
        //get version string from game manager
        version = GameManager.Instance.Version;

        //connect to Photon // the following line checks if this client was just created (and not yet online). if so, we connect
        if ( PhotonNetwork.connectionStateDetailed == PeerState.PeerCreated )
        {
            // Connect to the photon master-server. We use the settings saved in PhotonServerSettings (a .asset file in this project)
            PhotonNetwork.ConnectUsingSettings( version );
        }
        Debug.Log( "connecting..." );
    }

    public void CreateRoom ( string name)
    {
        GameObject cr = GameObject.Find( name );

        Text NameText = cr.transform.FindChild( "NAMEInputField" ).FindChild( "Text" ).GetComponentInChildren<Text>();
        Text PlayerText = cr.transform.FindChild( "PlayerCountInputField" ).FindChild("Text").GetComponentInChildren<Text>();
        Text Error = cr.transform.FindChild( "Error" ).GetComponentInChildren<Text>();

        Text playerName = GameObject.Find( "PlayerName" ).transform.FindChild( "Text" ).GetComponentInChildren<Text>();
        
        if ( playerName.text == "" )
        {
            Error.text = "Please enter a player name.";
            return;
        }

        GameManager.Instance.playerName = playerName.text;

        if ( NameText.text == "" )
        {
            Error.text = "Please enter a room name.";
            return;
        }

        if ( PlayerText.text == "" )
        {
            Error.text = "Please enter number of players.";
            return;
        }

        int playerCount;

        if ( int.TryParse( PlayerText.text, out playerCount ) )
        {
            if ( playerCount > 10 || playerCount < 1 )
            {
                Error.text = "Player count must be between 1 and 10.";
                return;
            }
        }
        else
        {
            Error.text = "Player count must be a number";
            return;
        }

        RoomOptions roomOptions = new RoomOptions();
        roomOptions.maxPlayers = (byte)playerCount;
        roomOptions.isVisible = true;
        roomOptions.isOpen = true;

        PhotonNetwork.CreateRoom( NameText.text, roomOptions, null );
        Debug.Log( "Created room..." );
    }

    void OnJoinedRoom ()
    {
        PhotonNetwork.LoadLevel( "scene1" );
    }

    void OnPhotonCreateRoomFailed ()
    {
        Debug.Log( "failed room creation" );
    }

    void OnJoinedLobby ()
    {
        Debug.Log( "Lobby joined..." );

        //we joined a lobby
        joined = true;

        if ( PhotonNetwork.insideLobby )
        {
            Debug.Log( "inside lobby" );
        }

        drawLobby();
    }

    void OnReceivedRoomListUpdate ()
    {
        listChanged = true;
    }

    void Update ()
    {
        if ( listChanged )
        {
            drawLobby();

            listChanged = false;
        }
    }

    void OnFailedToConnectToPhoton ( DisconnectCause cause )
    {
        //print error with reason for failure to join lobby
        Debug.LogError( "Failed to connect to photon, cause: " + cause.ToString() );
    }

    void OnGUI ()
    {
        if ( !joined )
        {
            //we're still connecting display loading icon
        }
        else if ( joined )
        {
            //we're connected time to draw lobby
        }
    }

    void drawLobby ()
    {
        Debug.Log( "drawLobby" );

        foreach(Transform child in container.transform)
        {
            GameObject.Destroy( child.gameObject );
        }

        if ( PhotonNetwork.GetRoomList().Length == 0 )
        {
            GameObject ri = Instantiate( roomItem ) as GameObject;
            ri.transform.FindChild( "PlayerCount" ).GetComponent<Text>().text = "";
            ri.transform.FindChild( "SelectRoom" ).gameObject.SetActive( false );
            ri.transform.FindChild( "RoomName" ).GetComponent<Text>().text = "There are no rooms. Create one below.";
            ri.transform.SetParent( container.transform, false );
        }
        else
        {
            float count = roomItem.transform.position.y;
            foreach ( RoomInfo room in PhotonNetwork.GetRoomList() )
            {
                //draw room info to GUI
                GameObject ri = Instantiate( roomItem ) as GameObject;
                ri.transform.FindChild( "PlayerCount" ).GetComponent<Text>().text = room.playerCount + "/" + room.maxPlayers;
                ri.transform.FindChild( "RoomName" ).GetComponent<Text>().text = room.name;
                ri.transform.position = new Vector3( ri.transform.position.x, count, ri.transform.position.z );

                string name = room.name;
                ri.transform.FindChild( "SelectRoom" ).GetComponent<Button>().onClick.AddListener( delegate { JoinRoom(name ); } );
                ri.transform.SetParent( container.transform, false );

                count -= 50;
            }
        }
        container.sizeDelta = new Vector2( 0, 50.0f * PhotonNetwork.GetRoomList().Length );
    }

    public void JoinRoom ( string name)
    {
        Debug.Log( name );

        Text Error = GameObject.Find( "Error" ).GetComponentInChildren<Text>();

        Text playerName = GameObject.Find( "PlayerName" ).transform.FindChild( "Text" ).GetComponentInChildren<Text>();

        if ( playerName.text == "" )
        {
            Error.text = "Please enter a player name.";
            return;
        }

        GameManager.Instance.playerName = playerName.text;

        PhotonNetwork.JoinRoom( name );
    }
}
