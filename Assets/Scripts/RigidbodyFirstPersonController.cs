using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using System.Collections;

using Photon;


[RequireComponent( typeof( Rigidbody ) )]
[RequireComponent( typeof( CapsuleCollider ) )]
public class RigidbodyFirstPersonController : Photon.MonoBehaviour
{
    [Serializable]
    public class MovementSettings
    {
        public float ForwardSpeed = 8.0f;   // Speed when walking forward
        public float BackwardSpeed = 4.0f;  // Speed when walking backwards
        public float StrafeSpeed = 4.0f;    // Speed when walking sideways
        public float RunMultiplier = 2.0f;   // Speed when sprinting
        public KeyCode RunKey = KeyCode.LeftShift;
        public float JumpForce = 30f;
        public AnimationCurve SlopeCurveModifier = new AnimationCurve( new Keyframe( -90.0f, 1.0f ), new Keyframe( 0.0f, 1.0f ), new Keyframe( 90.0f, 0.0f ) );
        [HideInInspector]
        public float CurrentTargetSpeed = 8f;

#if !MOBILE_INPUT
        private bool m_Running;
#endif

        public void UpdateDesiredTargetSpeed ( Vector2 input )
        {
            if ( input == Vector2.zero ) return;
            if ( input.x > 0 || input.x < 0 )
            {
                //strafe
                CurrentTargetSpeed = StrafeSpeed;
            }
            if ( input.y < 0 )
            {
                //backwards
                CurrentTargetSpeed = BackwardSpeed;
            }
            if ( input.y > 0 )
            {
                //forwards
                //handled last as if strafing and moving forward at the same time forwards speed should take precedence
                CurrentTargetSpeed = ForwardSpeed;
            }
#if !MOBILE_INPUT
            if ( Input.GetKey( RunKey ) )
            {
                CurrentTargetSpeed *= RunMultiplier;
                m_Running = true;
            }
            else
            {
                m_Running = false;
            }
#endif
        }

#if !MOBILE_INPUT
        public bool Running
        {
            get { return m_Running; }
        }
#endif
    }


    [Serializable]
    public class AdvancedSettings
    {
        public float groundCheckDistance = 0.01f; // distance for checking if the controller is grounded ( 0.01f seems to work best for this )
        public float stickToGroundHelperDistance = 0.5f; // stops the character
        public float slowDownRate = 20f; // rate at which the controller comes to a stop when there is no input
        public bool airControl; // can the user control the direction that is being moved in the air
    }


    public Camera cam;
    public MovementSettings movementSettings = new MovementSettings();
    public MouseLook mouseLook = new MouseLook();
    public AdvancedSettings advancedSettings = new AdvancedSettings();


    private Rigidbody m_RigidBody;
    private CapsuleCollider m_Capsule;
    private float m_YRotation;
    private Vector3 m_GroundContactNormal;
    private bool m_Jump, m_PreviouslyGrounded, m_Jumping, m_IsGrounded;

    private float lastSynchronizationTime = 0f;
    private float syncDelay = 0f;
    private float syncTime = 0f;
    private Vector3 syncStartPosition = Vector3.zero;
    private Vector3 syncEndPosition = Vector3.zero;

    public bool inGlue = false;
    public Collider glueCol;
    public float fSpeed;
    public float bSpeed;
    public float sSpeed;

    //public float ForwardSpeed = 8.0f;   // Speed when walking forward
    //public float BackwardSpeed = 4.0f;  // Speed when walking backwards
    //public float StrafeSpeed = 4.0f;    // Speed when walking sideways

    Quaternion rotation = Quaternion.identity;
    Vector3 rotSpeed = Vector3.zero;

    public Animator anim;
    public GameObject player;

    Vector3 lastPosition;

    public Texture2D crosshairImage;

    public float gravTimer = 5.0f;
    private float tempGravTimer;

    public bool forceHit = false;
    public Vector3 forceVec = Vector3.zero;


    public Vector3 Velocity
    {
        get { return m_RigidBody.velocity; }
    }

    public bool Grounded
    {
        get { return m_IsGrounded; }
    }

    public bool Jumping
    {
        get { return m_Jumping; }
    }

    public bool Running
    {
        get
        {
#if !MOBILE_INPUT
            return movementSettings.Running;
#else
	            return false;
#endif
        }
    }


    private void Start ()
    {
        m_RigidBody = GetComponent<Rigidbody>();
        m_Capsule = GetComponent<CapsuleCollider>();
        mouseLook.Init( transform, cam.transform );
        anim = GetComponentInChildren<Animator>();
        m_RigidBody.sleepThreshold = 0f;
        if ( photonView.isMine )
        {
            player.GetComponentInChildren<SkinnedMeshRenderer>().enabled = false;
        }

        fSpeed = movementSettings.ForwardSpeed;
        bSpeed = movementSettings.BackwardSpeed;
        sSpeed = movementSettings.StrafeSpeed;
        tempGravTimer = gravTimer;
    }


    private void Update ()
    {
        if ( photonView.isMine )
        {
            RotateView();

            if ( CrossPlatformInputManager.GetButtonDown( "Jump" ) && !m_Jump )
            {
                m_Jump = true;
            }
        }

        if ( inGlue )
        {
            movementSettings.ForwardSpeed = fSpeed / 3;
            movementSettings.BackwardSpeed = bSpeed / 3;
            movementSettings.StrafeSpeed = sSpeed / 3;
        }

        if ( !inGlue )
        {
            movementSettings.ForwardSpeed = fSpeed;
            movementSettings.BackwardSpeed = bSpeed;
            movementSettings.BackwardSpeed = sSpeed;
        }

        if ( m_RigidBody.useGravity == false )
        {
            advancedSettings.airControl = false;
            m_RigidBody.position += new Vector3( 0, 0.05f, 0 );
            gravTimer -= Time.deltaTime;
            if ( gravTimer < 0 )
            {
                m_RigidBody.useGravity = true;
                gravTimer = tempGravTimer;
                advancedSettings.airControl = true;
            }
        }
    }


    private void FixedUpdate ()
    {
        if ( photonView.isMine )
        {
            GroundCheck();
            Vector2 input = GetInput();

            if ( (Mathf.Abs( input.x ) > float.Epsilon || Mathf.Abs( input.y ) > float.Epsilon) && (advancedSettings.airControl || m_IsGrounded) )
            {
                // always move along the camera forward as it is the direction that it being aimed at
                Vector3 desiredMove = cam.transform.forward * input.y + cam.transform.right * input.x;
                desiredMove = Vector3.ProjectOnPlane( desiredMove, m_GroundContactNormal ).normalized;

                desiredMove.x = desiredMove.x * movementSettings.CurrentTargetSpeed;
                desiredMove.z = desiredMove.z * movementSettings.CurrentTargetSpeed;
                desiredMove.y = desiredMove.y * movementSettings.CurrentTargetSpeed;
                if ( m_RigidBody.velocity.sqrMagnitude <
                    (movementSettings.CurrentTargetSpeed * movementSettings.CurrentTargetSpeed) )
                {
                    m_RigidBody.AddForce( desiredMove * SlopeMultiplier(), ForceMode.Impulse );
                }
            }

            if ( m_IsGrounded )
            {
                m_RigidBody.drag = 5f;

                if ( m_Jump )
                {
                    m_RigidBody.drag = 0f;
                    m_RigidBody.velocity = new Vector3( m_RigidBody.velocity.x, 0f, m_RigidBody.velocity.z );
                    m_RigidBody.AddForce( new Vector3( 0f, movementSettings.JumpForce, 0f ), ForceMode.Impulse );
                    m_Jumping = true;
                }

                if ( !m_Jumping && Mathf.Abs( input.x ) < float.Epsilon && Mathf.Abs( input.y ) < float.Epsilon && m_RigidBody.velocity.magnitude < 1f )
                {
                    m_RigidBody.Sleep();
                }
            }
            else
            {
                m_RigidBody.drag = 0f;
                if ( m_PreviouslyGrounded && !m_Jumping )
                {
                    StickToGroundHelper();
                }
            }
            m_Jump = false;
            anim.SetFloat( "Speed", Input.GetAxis( "Vertical" ) );
            anim.SetFloat( "Direction", Input.GetAxis( "Horizontal" ) );

            if ( forceHit )
            {
                //forceVec = new Vector3(UnityEngine.Random.Range(-1000.0F, 1000.0F), UnityEngine.Random.Range(0, 100.0F), UnityEngine.Random.Range(-1000.0F, 1000.0F));
                Debug.Log( "hit : " + PhotonNetwork.playerName );
                m_RigidBody.AddForce( forceVec, ForceMode.Impulse );
                forceHit = false;
            }


            //if we think we're still in glue but the glue we are supposedly colliding with no longer exists
            if(inGlue && !glueCol)
            {
                //then set us to not be in glue
                inGlue = false;
            }

        }
        else
        {
            SyncedMovement();
        }

    }

    [PunRPC]
    void ForcePush ( bool hit, Vector3 direction )
    {
        forceVec = direction * 1000;
        forceHit = hit;
        Debug.Log( "Push" );
    }

    [PunRPC]
    void ForcePull ( bool hit, Vector3 direction )
    {
        forceVec = -direction * 1000;
        forceHit = hit;
        Debug.Log( "Pull" );
    }

    private void SyncedMovement ()
    {
        syncTime += Time.deltaTime;
        m_RigidBody.position = Vector3.Lerp( syncStartPosition, syncEndPosition, syncTime / syncDelay );
        m_RigidBody.rotation = Quaternion.Lerp( m_RigidBody.rotation, rotation, syncTime / syncDelay );
        m_RigidBody.angularVelocity = Vector3.Lerp( m_RigidBody.angularVelocity, rotSpeed, syncTime / syncDelay );
    }

    void OnPhotonSerializeView ( PhotonStream stream, PhotonMessageInfo info )
    {
        if ( stream.isWriting )
        {
            stream.SendNext( m_RigidBody.position );
            stream.SendNext( m_RigidBody.velocity );
            stream.SendNext( m_RigidBody.rotation );
            stream.SendNext( m_RigidBody.angularVelocity );
            stream.SendNext( anim.GetFloat( "Speed" ) );
            stream.SendNext( anim.GetFloat( "Direction" ) );
        }
        else
        {
            Vector3 syncPosition = (Vector3)stream.ReceiveNext();
            Vector3 syncVelocity = (Vector3)stream.ReceiveNext();

            rotation = (Quaternion)stream.ReceiveNext();
            rotSpeed = (Vector3)stream.ReceiveNext();

            anim.SetFloat( "Speed", (float)stream.ReceiveNext() );
            anim.SetFloat( "Direction", (float)stream.ReceiveNext() );

            syncTime = 0f;
            syncDelay = Time.time - lastSynchronizationTime;
            lastSynchronizationTime = Time.time;

            syncEndPosition = syncPosition + syncVelocity * syncDelay;
            syncStartPosition = m_RigidBody.position;
        }
    }

    void OnPhotonInstantiate ( PhotonMessageInfo info )
    {
        gameObject.name = info.sender.name;
    }

    void OnGUI ()
    {
        float xMin = (Screen.width / 2) - (crosshairImage.width / 2);
        float yMin = (Screen.height / 2) - (crosshairImage.height / 2);
        GUI.DrawTexture( new Rect( xMin, yMin, crosshairImage.width, crosshairImage.height ), crosshairImage );
    }

    private float SlopeMultiplier ()
    {
        float angle = Vector3.Angle( m_GroundContactNormal, Vector3.up );
        return movementSettings.SlopeCurveModifier.Evaluate( angle );
    }

    private void StickToGroundHelper ()
    {
        RaycastHit hitInfo;
        if ( Physics.SphereCast( transform.position, m_Capsule.radius, Vector3.down, out hitInfo,
                               ((m_Capsule.height / 2f) - m_Capsule.radius) +
                               advancedSettings.stickToGroundHelperDistance ) )
        {
            if ( Mathf.Abs( Vector3.Angle( hitInfo.normal, Vector3.up ) ) < 85f )
            {
                m_RigidBody.velocity = Vector3.ProjectOnPlane( m_RigidBody.velocity, hitInfo.normal );
            }
        }
    }

    private Vector2 GetInput ()
    {

        Vector2 input = new Vector2
        {
            x = Input.GetAxis( "Horizontal" ),
            y = Input.GetAxis( "Vertical" )
        };
        movementSettings.UpdateDesiredTargetSpeed( input );
        return input;
    }

    [PunRPC]
    void setGravity ()
    {
        m_RigidBody.useGravity = false;
    }

    private void RotateView ()
    {
        //avoids the mouse looking if the game is effectively paused
        if ( Mathf.Abs( Time.timeScale ) < float.Epsilon ) return;

        // get the rotation before it's changed
        float oldYRotation = transform.eulerAngles.y;

        mouseLook.LookRotation( transform, cam.transform );

        if ( m_IsGrounded || advancedSettings.airControl )
        {
            // Rotate the rigidbody velocity to match the new direction that the character is looking
            Quaternion velRotation = Quaternion.AngleAxis( transform.eulerAngles.y - oldYRotation, Vector3.up );
            m_RigidBody.velocity = velRotation * m_RigidBody.velocity;
        }
    }

    /// sphere cast down just beyond the bottom of the capsule to see if the capsule is colliding round the bottom
    private void GroundCheck ()
    {
        m_PreviouslyGrounded = m_IsGrounded;
        RaycastHit hitInfo;
        if ( Physics.SphereCast( transform.position, m_Capsule.radius, Vector3.down, out hitInfo,
                               ((m_Capsule.height / 2f) - m_Capsule.radius) + advancedSettings.groundCheckDistance ) )
        {
            m_IsGrounded = true;
            m_GroundContactNormal = hitInfo.normal;
        }
        else
        {
            m_IsGrounded = false;
            m_GroundContactNormal = Vector3.up;
        }
        if ( !m_PreviouslyGrounded && m_IsGrounded && m_Jumping )
        {
            m_Jumping = false;
        }
    }

    public void OnTriggerEnter ( Collider other )
    {
        if ( other.tag == "GLUE" )
        {
            inGlue = true;

            //set our gleuCol to other for self.Destroy issue
            glueCol = other;
        }
    }

    public void OnTriggerExit ( Collider other )
    {
        if ( other.tag == "GLUE" )
        {
            inGlue = false;
        }

    }

    public void OnCollisionEnter ( Collision collision )
    {
        if ( collision.collider.name == "ammo_Force(Clone)" )
        {
            m_RigidBody.AddForce( collision.collider.GetComponent<Rigidbody>().velocity * 100 );
        }
    }

    public void OnCollisionExit ( Collision collision )
    {
    }


    // add player event to players queue to be displaeyd on screen
    [PunRPC]
    void AddPlayerEvent ( string playerHit, fire.types t )
    {
        if ( photonView.isMine )
        {
            switch ( t )
            {
                case fire.types.force:
                    GameObject.Find( "HitUpdates" ).GetComponent<PlayerEvents>().textQueue.Enqueue( playerHit + " hit you with force." );
                    break;
                case fire.types.forcepull:
                    GameObject.Find( "HitUpdates" ).GetComponent<PlayerEvents>().textQueue.Enqueue( playerHit + " pulled you with force." );
                    break;
                case fire.types.Grav:
                    GameObject.Find( "HitUpdates" ).GetComponent<PlayerEvents>().textQueue.Enqueue( playerHit + " turned your gravity off." );
                    break;
            }

            GameObject.Find( "HitUpdates" ).GetComponent<PlayerEvents>().updateDisplay();
        }
    }

    [PunRPC]
    void PlayerCollectedCube ( string player, string color )
    {
        switch ( color )
        {
            case "Red":
                GameObject.Find( "HitUpdates" ).GetComponent<PlayerEvents>().textQueue.Enqueue( player + " collected the red cube." );
                GameManager.Instance._greenTeamScore += 1;
                break;
            case "Green":
                GameObject.Find( "HitUpdates" ).GetComponent<PlayerEvents>().textQueue.Enqueue( player + " collected the green cube." );
                GameManager.Instance._redTeamScore += 1;
                break;
        }

        GameObject.Find( "HitUpdates" ).GetComponent<PlayerEvents>().updateDisplay();
    }
}

