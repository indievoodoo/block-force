using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class WeaponManager : Photon.MonoBehaviour
{
    private GameObject player;
    private GameObject GunPlaceHolder;
    private GameObject PickAxe;
    private GameObject ForceGun;
    private GameObject ForcePull;
    private GameObject GlueGun;
    private GameObject GravGun;
    private GameObject controller;
    private GameObject SelectedWeapon;

    public AudioSource gunchangeSound;

    void OnEnable ()
    {
        player = GameObject.Find( PhotonNetwork.playerName );
        GunPlaceHolder = player.transform.FindChild( "MainCamera" ).FindChild( "GunPlaceHolder" ).gameObject;

        PickAxe = GunPlaceHolder.transform.FindChild( "PickAxe" ).gameObject;

        ForceGun = GunPlaceHolder.transform.FindChild( "Force" ).gameObject;
        ForceGun.GetComponent<fire>().enabled = true;

        ForcePull = GunPlaceHolder.transform.FindChild("ForcePull").gameObject;
        ForcePull.GetComponent<fire>().enabled = true;

        GlueGun = GunPlaceHolder.transform.FindChild( "Glue" ).gameObject;
        GlueGun.GetComponent<fire>().enabled = true;

        GravGun = GunPlaceHolder.transform.FindChild("Grav").gameObject;
        GravGun.GetComponent<fire>().enabled = true;

        controller = GameObject.Find( "Controller" ).gameObject;

        gunchangeSound = GetComponent<AudioSource>();

        SelectedWeapon = GameObject.FindGameObjectWithTag( "SelectedWeapon" );
    }

    void Update ()
    { 
        //BLOCKS for now
        if ( Input.GetKeyUp( KeyCode.Alpha1 ) )
        {
            removeAllGuns();
            photonView.RPC( "setActive", PhotonTargets.AllViaServer, player.name, PickAxe.name, true );
            controller.GetComponent<MouseController>().enabled = true;
            SelectedWeapon.GetComponent<RectTransform>().anchoredPosition = new Vector2( 0, 0 );
            gunchangeSound.Play();
        }

        //force Push
        if ( Input.GetKeyUp( KeyCode.Alpha2 ) )
        {
            removeAllGuns();
            photonView.RPC( "setActive", PhotonTargets.AllViaServer, player.name, ForceGun.name, true );
            SelectedWeapon.GetComponent<RectTransform>().anchoredPosition = new Vector2( 100, 0 );
            gunchangeSound.Play();
        }

        //force pull
        if ( Input.GetKeyUp( KeyCode.Alpha3 ) )
        {
            removeAllGuns();
            photonView.RPC( "setActive", PhotonTargets.AllViaServer, player.name, ForcePull.name, true );
            SelectedWeapon.GetComponent<RectTransform>().anchoredPosition = new Vector2( 200, 0 );
            gunchangeSound.Play();
        }

        //glue Gun
        if (Input.GetKeyUp(KeyCode.Alpha4))
        {
            removeAllGuns();
            photonView.RPC("setActive", PhotonTargets.AllViaServer, player.name, GlueGun.name, true);
            SelectedWeapon.GetComponent<RectTransform>().anchoredPosition = new Vector2( 300, 0 );
            gunchangeSound.Play();
        }

        //grav Gun
        if (Input.GetKeyUp(KeyCode.Alpha5))
        {
            removeAllGuns();
            photonView.RPC("setActive", PhotonTargets.AllViaServer, player.name, GravGun.name, true);
            SelectedWeapon.GetComponent<RectTransform>().anchoredPosition = new Vector2( 400, 0 );
            gunchangeSound.Play();
        }
    }

    void removeAllGuns ()
    {
        //remove current gun
        foreach ( Transform child in GunPlaceHolder.transform )
        {
            photonView.RPC( "setActive", PhotonTargets.AllViaServer, player.name, child.gameObject.name, false );
        }

        controller.GetComponent<MouseController>().enabled = false;
    }

    [PunRPC]
    void setActive ( string mainObj, string childObj, bool value )
    {
        GameObject.Find( mainObj ).transform.FindChild( "MainCamera" ).FindChild( "GunPlaceHolder" ).FindChild( childObj ).gameObject.SetActive( value );
    }
}
